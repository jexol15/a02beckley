var App = {
	calcDays: function(){
    	var d1 = document.getElementsByName("fdate")[0].value;
        var d2 = document.getElementsByName("ldate")[0].value;
		var days = App.calcDaysHelp(d1, d2);
    	//changes a label with jquery
        $('#output').html(days + " days");
        //changes a label with javascript
        document.getElementById("outputm").innerHTML = (days * 86400000) + " milliseconds";
    },
    //testable function
    calcDaysHelp: function(d1, d2){
        var date1 = Date.parse(d1);
        var date2 = Date.parse(d2);
        var msec = date2 - date1;
        var days = msec / 86400000;
        return days;
    }
}