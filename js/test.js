QUnit.test("Test various dates", function (assert) {
	//test appropriate dates
    assert.equal(App.calcDaysHelp("March 10, 2010", "March 11, 2010"), 1);
    //test negative difference in dates
    assert.equal(App.calcDaysHelp("March 10, 2010", "March 9, 2010"), -1);
    //test invalid dates
    assert.notOk(App.calcDaysHelp("purple", "March 11, 2010"));
    //test test vague dates
    assert.equal(App.calcDaysHelp("1800", "1900"), 36524);
});